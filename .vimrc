set nocompatible              " be iMproved, required                                                                 
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'gmarik/Vundle.vim'
Plugin 'fatih/vim-go'
Plugin 'kien/ctrlp.vim'
Plugin 'valloric/YouCompleteMe'
Plugin 'godlygeek/tabular'
Plugin 'tpope/vim-surround'
" Plugin 'flowtype/vim-flow'
call vundle#end()            " required
filetype plugin on           " required
filetype plugin indent on    " required

syntax enable

" General settings
set backspace=2                  " Make backspace work like most other applications
set nu                           " Turn on line numbers
set autoindent                   " Turn on autoindent
set expandtab                    " Exapnd tabs into spaces
set tabstop=4                    " Tabs are 4 spaces
set shiftwidth=4                 " Shift by 4 spaces
set nowrap                       " Do not wrap lines
set clipboard=unnamed
set esckeys                      " No timeout for mapping escape keys
set timeoutlen=500               " Shorter map timeout (default 1000)
set ttimeoutlen=0                " Don't timeout after Esc

" Current line highlight
set cursorline
hi CursorLine cterm=NONE ctermbg=Black
set ruler

" Search settings
set hlsearch                     " Enable search highlighting
set incsearch
set ignorecase
set smartcase
noh 
hi Search guibg=Yellow guifg=Black ctermbg=Yellow ctermfg=Black
nnoremap <C-n> :noh<cr>

" Move lines up and down with ctrl + j|k
nnoremap <C-j> :m .+1<CR>==
nnoremap <C-k> :m .-2<CR>==
inoremap <C-j> <Esc>:m .+1<CR>==gi
inoremap <C-k> <Esc>:m .-2<CR>==gi
vnoremap <C-j> :m '>+1<CR>gv=gv
vnoremap <C-k> :m '<-2<CR>gv=gv

" Save with \s and quit with \q
nnoremap <leader>s :w<cr>
inoremap <leader>s <C-c>:w<cr>
nnoremap <leader>q :q<cr>

" Reload vimrc with \l
nnoremap <leader>l :source ~/.vimrc<cr>

" Exit insert mode with jk or kj
inoremap jk <Esc>
inoremap kj <Esc>

" Home and end with ctrl+ h or l
nnoremap <C-h> ^
nnoremap <C-l> $
inoremap <C-h> <Esc>I
inoremap <C-l> <Esc>A
vnoremap <C-h> ^
vnoremap <C-l> $

" Change tabs with Shift + h or l
nnoremap <S-l> gt
nnoremap <S-h> gT

" Fast macro with qq [macro] q Q
nnoremap Q @q

" Toggle paste mode with \z
nnoremap <leader>z :set paste!<cr>

" Misc.
set wildignore+=*/node_modules/* " Ignore node_modules (for ctrlp searching)

" GD specific
set wildignore+=*/bower/*        " Ignore node_modules (for ctrlp searching)
